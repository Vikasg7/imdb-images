@echo off

:: %1 = list of files
:: %2 = destination folder

for /F "tokens=*" %%a in (%1) do (
   mv %%a %2
)

echo Done!
@echo on