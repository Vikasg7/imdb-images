(function (require, module) {

   var promise = require("promise")
   require("thenForEach").extends(promise)

   var session = require("request").defaults({jar: true})
   var fs = require("fs")
   var format = require("util").format
   var $ = require("streamify")

   var rootDir = "output/Women/"

   var readline= {
      options: {
         objectMode: true
      },
      transform: function (data, enc, done) {
         var stream = this
         var lines = [stream.lastLine, data].join("").split(/\r?\n|\r(?!\n)/)
         stream.lastLine = lines.pop()
         lines.length && lines.forEach(function (line) { line && this.push(line) }, stream)
         done()
      },
      flush: function (done) {
         var stream = this
         if (stream.lastLine) { 
            stream.push(stream.lastLine)
            stream.lastLine = null
         }
         done()
      }
   }

   process.stdin
      .setEncoding("UTF-8")
      .pipe($(readline))
      .pipe($(init))
      .on("finish", function () { console.error("Done!") })

   function init(row, enc, getNextUrl) {
      var folder = row.split("|")[0].trim()
      var url = row.split("|")[1].trim()
      return (
         promise.resolve(row)
            .then(function () {
               var folPath = rootDir + folder
               if (ifExist(folPath)) { return }
               fs.mkdirSync(folPath)
               var options = {url: url, fPath: format("%s/%s.jpg", folPath, folder)}
               return download(options)
            })
            .catch(onError)
            .then(getNextUrl)
      )
   }

   function download(options) {
      var url = options.url  
      var fPath = options.fPath
      return new promise(function (resolve, reject) {
         session({url: url}, function (err, resp, body) {
            if (err) {
               reject(options)
            } else if ((resp.statusCode !== 200) && (resp.statusCode !== 201)) {
               reject(options)
            } else {
               resolve()
            } 
         })
         .on("error", function () {
            reject(options)
         })
         .pipe(fs.createWriteStream(fPath))
      })//.then(wait)
   }

   function onError(error) {
      console.log(error)
   }

   function wait() {
      var seconds = 0.5
      return new promise(function (resolve, reject) {
         setTimeout(resolve, seconds * 1000)
      })
   }

   function ifExist(fPath) {
      try {
         return fs.statSync(fPath)
      } catch (e) {}
      return false
   }

})(require, module)