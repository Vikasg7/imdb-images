(function (require, module) {

   var promise = require("promise")
   require("thenForEach").extends(promise)

   var session = require("request").defaults({jar: true})
   var fs = require("fs")
   var format = require("util").format
   var whacko = require("whacko")

   init()

   function init() {
      // creating a session on imdb website ie. generating cookies
      get({url: "http://www.imdb.com"})
         .then(function () {
            var arr = []
            for (var i = 0; i < 6; i++) {
               arr.push(i)
            }
            return arr
         })
         .thenForEach(Scrape, onError)
         .then(function () { console.log("Finished: %d", parseInt(process.uptime() / 60)) })
         .catch(console.log)
   }

   function Scrape(item, index, context) {
      var options = {
         url: "http://www.imdb.com/search/name?count=50&death_date=,2017&gender=female&start=" + ((50 * item) + 1),
         method: "GET",
         headers: {
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36",
            "Host": "www.imdb.com"
         }
      }

      return (
         promise
            .resolve(options)
            .then(get)
            .then(getImageUrls)
      )
   }

   function get(options) {
      return new promise( function (resolve, reject) {
         session(options, function (err, resp, body) {
            if (err) {
               reject(err.code)
            } else if ((resp.statusCode !== 200) && (resp.statusCode !== 201)) {
               reject(resp.statusCode)
            } else {
               resolve({
                  body: body,
                  parsed: whacko.load(body)
               })
            }
         })
      })
   }

   function getImageUrls(resp) {
      var $ = resp.parsed
      var rows = $("table tr.detailed")
      if (!rows.length) { throw "table not found" }

      $(rows).each(iterate)
      function iterate(i, row) {
         var name = $(row).find("td.image>a").attr("title").trim().replace(/ /g, "_")
         var imageUrl = $(row).find("td.image>a>img").attr("src")
         console.log("%s\t%s", name, imageUrl)
      }
   }

   function onError(item, index, error) {
      console.log(error)
      console.log("%s - %s", index, error.toString())
   }

   function wait() {
      var seconds = 2
      return new promise(function (resolve, reject) {
         setTimeout(resolve, seconds * 1000)
      })
   }

   function ifExist(fPath) {
      try {
         return fs.statSync(fPath).isDirectory()
      } catch (e) {}
      return false
   }

})(require, module)